// meetupcomments/dao/comment.go

// Data Access Objects are responsible for accessing comments data in the database.

// LICENSE GNU GPL v3.0

//--------------------------------------------------------------------------------//

package dao

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"gitlab.com/ryanthebossross/meetupcomments/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// get database name from env
var dbName = os.Getenv("dbNameENV")

// get collection name from env
var collection = os.Getenv("collectionENV")

// get the connection uri from the env
var dbConnection = os.Getenv("dbConnectionENV")

// use bytes.TrimSpace() for secrets

// init var that's a pointer to the database object
var db *mongo.Database

// run init first
func init() {

	// set up connection to the database with the dbConnection var and check for errors
	client, err := mongo.NewClient(options.Client().ApplyURI(dbConnection))
	if err != nil {
		log.Fatal(err)
	}
	// set the dial context to x seconds
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// connect to db and check for the error
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	// shorten connection object
	db = client.Database(dbName)
}

// SeedDB handler seeds the database with dummy data for dev/demo purposes. NOT MEANT FOR PRODUCTION.
func SeedDB() {
	// drop collection to clear data and check error
	err := db.Collection(collection).Drop(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	// debug message
	fmt.Println("dropped db.")

	//init comments type as slice of the Comment model
	var comments []models.Comment

	// read seed data file
	seedData, err := ioutil.ReadFile("seedData.json")
	if err != nil {
		log.Fatal(err)
	}
	// unmarshal json objects into Golang struct
	json.Unmarshal(seedData, &comments)
	// create a slice of empty interfaces to store each struct object
	var com []interface{}
	for _, c := range comments {
		com = append(com, c)
	}
	// insert each comments stuct objects into the db and check for an error
	_, err = db.Collection(collection).InsertMany(context.Background(), com)
	if err != nil {
		log.Fatal(err)
	}
	// debug message
	fmt.Println("db seeded!")
}

// InsertOneComment inserts one item from the Comment model
func InsertOneComment(c models.Comment) {
	// insert comment and check for the error
	insertResult, err := db.Collection(collection).InsertOne(context.Background(), c)
	if err != nil {
		log.Panic(err)
	}
	// log the id of the document inserted
	log.Println("Inserted a single document: ", insertResult.InsertedID)
}

// GetAllComments returns all comments from DB
func GetAllComments() []models.Comment {
	// init slice of comments model
	var comments []models.Comment

	// init single model object
	var com models.Comment

	// get all commnets from the db and check for error
	cur, err := db.Collection(collection).Find(context.Background(), bson.D{}, nil)
	if err != nil {
		log.Fatal(err)
	}

	// Loop over the cursor from the db, check for an error, and append the error free object to the slice of objects
	for cur.Next(context.Background()) {
		err := cur.Decode(&com)
		if err != nil {
			log.Fatal(err)
		}
		comments = append(comments, com)
	}
	// check the error
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// close the connection
	cur.Close(context.Background())

	// return comments object
	return comments
}
