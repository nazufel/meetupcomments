package dao

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// PingDB handler is a readiness probe for kubernetes to check if the app has a connection to the DB.
func PingDB() error {
	// set up connection to the database with the dbConnection var and check for errors
	client, err := mongo.NewClient(options.Client().ApplyURI(dbConnection))
	if err != nil {
		log.Fatal(err)
	}
	// set the dial context to x seconds
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// connect to db and check for the error
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	// shorten connection object
	db = client.Database(dbName)

	// ping the db to see if the app has a connection and handle the error
	err = client.Ping(ctx, nil)
	if err != nil {
		return err
	}
	// return the nil error
	return err
}
