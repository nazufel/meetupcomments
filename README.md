# Meetup Comments
Meetup Comments is an application developed as the demo app for the [Cinciannti Cloud Native](https://www.meetup.com/Cincinnati-Cloud-Native/) April 2019 Meetup where fictional users are able to live comment during the Meetup's talk. The other code for the talk can be found [here](https://gitlab.com/ryanthebossross/2019-april-cincinnati-cloud-native-docker-talk). 

The app is written in [Golang](https://golang.org) with a [MongoDB](https://mongodb.com) backend. The app is structured to support the [7 RESTful Routes](https://medium.com/@shubhangirajagrawal/the-7-restful-routes-a8e84201f206), though only three are implimented. It's not a full CRUD app and could with more time, but it's not meant to be used after the meetup. I didn't feel the need to spend any more time than necessary.

## Cloud Native, Docker, and Implimentation
Meetup Comments is a Cloud Native application. Ken Owens said it best in an [artcle](https://www.cncf.io/blog/2017/05/15/developing-cloud-native-applications/) on the CNCF blog of what Cloud Native means, "The new cloud native pattern consists of microservices architecture, containerized services, and distributed management and orchestration." This application was built from the ground up to be able to run in a microservices architecture, containerized with [Docker](https://docker.com), and have distributed management and orchestration. 

The goal was to create an application where the implimentation details are abstracted away. The app looks to the environment for configurations that are injected at runtime. It doesn't matter if this app is ran by [Docker Compose](https://docs.docker.com/compose/) or [Docker Swarm](https://docs.docker.com/swarm/). Moving application code through the SDLC of dev, test, stage, production, etc. is as simple as runtime environment configuration done by the orchestrater. Runtime variables control the application. Developers write business logic and Operaters write the control plane code.

I have not ran this service on [Kubernetes](https://kubernetes.io/), yet, but in theory that should be possible without changing application code.

### Docker Hub
Docker Images created out of this project can be found on the [Docker Hub](https://hub.docker.com/r/ryanthebossross/meetupcomments).

## License
This project is licnesed under the GNU GPL v3.0 license. You can find the license [here](https://gitlab.com/ryanthebossross/meetupcomments/blob/master/LICENSE) for more information.

## Structure
The structure of this project is meant to be minimalist and modular. There are several Go packages: ```dao```, ```handlers```, and ```models```. There a single ```infra``` directory holds all of the infratrucutre code such as the ```Vagrantfile``` to build vms and deployment code for Docker Compose and Docker Swarm. The ```templates``` directory holds templates to be rendered by the front end.

## Kubernetes

### Kubernetes Dashboard

Used this [article](https://github.com/kubernetes/dashboard) to deploy the dashboard.

#### Command for Bearer Token
`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')`

## TODO
* readiness and livelness probes

## Maintainer
Ryan Ross <ryanthebossross@gmail.com>
