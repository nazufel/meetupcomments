// meetupcomments/models/comment.go

// Defines stucts for the Comments models

// LICENSE GNU GPL v3.0

//--------------------------------------------------------------------------------//

package models

import "time"

// Comment struct to hold text data
type Comment struct {
	Author  string    `bson:"author" json:"author" form:"author"`
	Text    string    `bson:"text" json:"text" form:"text"`
	Created time.Time `json:"created,omitempty" bson:"created"`
	Updated time.Time `json:"updated,omitempty" bson:"updated"`
}
