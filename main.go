// meetupcomments/main.go

// MAINTAINER: Ryan Ross - ryanthebossross@gmail.com

// PURPOSE: Web app for fictional users to leave comments during the 2019 April Cincinnati Cloud Native Meetup talk.

// Main function to boot strap the rest of the app with routes and the web server.

// LICENSE GNU GPL v3.0

//--------------------------------------------------------------------------------//

// define the package
package main

// import other packages
import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/ryanthebossross/meetupcomments/dao"
	"gitlab.com/ryanthebossross/meetupcomments/routes"
)

// look to env for port
var servicePort = os.Getenv("servicePortENV")

// init stuff before starting the web server
func init() {
	// check if the proper env vars are exported.
	if os.Getenv("stageENV") != "testing" {
		checkENV()
	}

	// seed the database with dummy data only if in development stage
	if os.Getenv("stageENV") == "development" {
		dao.SeedDB()
	}
}

// main function
func main() {

	// print the server has started
	fmt.Println("Started the server and running on port: " + servicePort)

	// start the webserver and listen on a port and use the routes package
	http.ListenAndServe(":"+servicePort, routes.Routes())
}

// checkENV func checks for specific env's that are needed to run the app
func checkENV() {

	// message table composite literal holds env var to look for and debug message if not found
	mt := []struct {
		env     string
		message string
	}{
		{"stageENV", "Could not find stageENV variable. Please export stageENV what stage the app is in. Valid stages are: development, test, or production."},
		{"dbNameENV", "Could not find dbNameENV variable. Please export dbNameENV of the database to use."},
		{"dnsNameENV", "Could not find dnsNameENV variable. Please export dnsNameENV to the routable DNS name of the host and stage."},
		{"collectionENV", "Could not find collectionENV variable. Please export collectionENV to the collection you are using in MongoDB."},
		{"servicePortENV", "Could not find servicePortENV. Please export servicePortENV so the server knows what port to listen on."},
	}

	// message check loops over message struct checking for the env var and logs debug message if not found
	for _, mc := range mt {
		if len(os.Getenv(mc.env)) == 0 {
			log.Fatal(mc.message)
		}
	}
	fmt.Println("All variables are exported. Starting the server.")
}
