package routes

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/ryanthebossross/meetupcomments/handlers"
)

// Routes holds all the routes for comments
func Routes() http.Handler {

	// init Gorilla Mux router
	r := mux.NewRouter()

	////////////////////
	// RESTful routes //
	////////////////////

	// INDEX site - GET
	r.HandleFunc("/", handlers.GetIndex).Methods("GET")

	// INDEX comments - GET
	r.HandleFunc("/comments", handlers.GetAllComments).Methods("GET")

	// CREATE comment - POST
	r.HandleFunc("/comments", handlers.PostComment).Methods("POST")

	// Liveness - GET
	r.HandleFunc("/liveness", handlers.Liveness).Methods("GET")

	// Readiness - GET
	r.HandleFunc("/readiness", handlers.Readiness).Methods("GET")

	// return the router object
	return r
}
