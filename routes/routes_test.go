package routes

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

// TestPostRoutes tests the routing behavior if the request is a GET
func TestGetRoutes(t *testing.T) {

	// table test struct
	tt := []struct {
		name         string
		route        string
		expectedCode int
		eMessage     string
	}{
		{"GET Comments Route", "/comments", 200, "Expected status OK, got: %s"},
		{"GET Comments Route", "/badroute", 404, "Expected status Not Found, got: %s"},
	}
	// start up a new server to test routes
	srv := httptest.NewServer(Routes())

	// defer shutting the server down
	defer srv.Close()

	// test check loops over test struct checking if routes respond with expected status codes.
	for _, tc := range tt {
		res, err := http.Get(fmt.Sprintf("%s/%s", srv.URL, tc.route))
		if err != nil {
			t.Fatalf("Clould not send GET request: %v", err)
		}
		if res.StatusCode != tc.expectedCode {
			t.Fatalf(tc.eMessage, res.StatusCode)
		}
	}
}

// // TestPostRoutes tests the routing behavior if the request if a POST
// func TestPostRoutes(t *testing.T) {

// 	// table test struct
// 	tt := []struct {
// 		name         string
// 		method       string
// 		route        string
// 		expectedCode int
// 		eMessage     string
// 	}{
// 		{"POST Comments Route", "POST", "/comments", 200, "Expected status OK, got: %s"},
// 		{"POST Comments Route", "POST", "/badroute", 404, "Expected status Not Found, got: %s"},
// 	}

// 	reader := strings.NewReader(`{"Author:" "Snape", "Text": "Containers are for postions, not code."}`)
// 	// start up a new server to test routes
// 	srv := httptest.NewServer(Routes())

// 	// defer shutting the server down
// 	defer srv.Close()

// 	// test check loops over test struct checking if routes respond with expected status codes.
// 	for _, tc := range tt {
// 		err := http.NewRequest(tc.method, tc.route, reader)
// 		if err != nil {
// 			t.Fatalf("Clould not send GET request: %v", err)
// 		}
// 		if res.StatusCode != tc.expectedCode {
// 			t.Fatalf(tc.eMessage, res.StatusCode)
// 		}
// 	}
// }
