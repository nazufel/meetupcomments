// handlers/health.go

// handlers for Kubernetes Liveness and Readiness Probes

package handlers

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/ryanthebossross/meetupcomments/dao"
)

// Liveness handler responds with 200 header if server is running
func Liveness(w http.ResponseWriter, r *http.Request) {
	// only print the Liveness Probe debug message in dev
	if os.Getenv("stageENV") == "development" {
		log.Println("Hit Liveness Handler")
	}
	// write back response of 200 and "ok"
	w.WriteHeader(200)
	w.Write([]byte("ok"))
}

// Readiness handler renders the /comments index template
func Readiness(w http.ResponseWriter, r *http.Request) {
	// only print the Readiness Probe debug message in dev
	if os.Getenv("stageENV") == "development" {
		log.Println("Hit Readiness Handler")
	}
	// Pring the DB to see if the app can access it
	err := dao.PingDB()
	// if the returned error is not nil, send 500 because the DB is unavailable
	if err != nil {
		// error message
		em := "No connection to the database"
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError) // 500
		w.Write([]byte(em))
		log.Println(em)
	} else {
		// write back response of 200, "ok" and log
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		w.Write([]byte("ok"))
	}
}
