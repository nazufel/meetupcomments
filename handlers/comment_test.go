package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

// func TestGetIndex(t *testing.T) {

// 	req, err := http.NewRequest("GET", "localhost:8000/", nil)
// 	if err != nil {
// 		t.Fatalf("could not create the request: %v", err)
// 	}
// 	rec := httptest.NewRecorder()

// 	GetIndex(rec, req)

// 	res := rec.Result()
// 	if res.StatusCode != http.StatusMovedPermanently {
// 		t.Errorf("expected Status Moved Permanently; got %v", err)
// 	}
// }

// TestGetIndex tests and GetIndex func to see if the request is redirected
func TestGetIndexHandlerStatusCodes(t *testing.T) {

	tt := []struct {
		name         string
		route        string
		method       string
		expectedCode int
	}{
		{"GET Index", "/", "GET", 301},
		{"POST Index", "/", "POST", 301},
		{"PUT Index", "/", "PUT", 301},
		{"PATCH Index", "/", "PATCH", 301},
		{"DELETE Index", "/", "DELETE", 301},
	}

	// message check loops over message struct checking for the env var and logs debug message if not found
	for _, tc := range tt {
		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest(tc.method, tc.route, nil)
		if err != nil {
			t.Fatal(err)
		}

		// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(GetIndex)

		// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
		// directly and pass in our Request and ResponseRecorder.
		handler.ServeHTTP(rr, req)
		if rr.Code != tc.expectedCode {
			t.Errorf(tc.name+" test case: handler returned wrong status code: got %v want %v", rr.Code, tc.expectedCode)
		}
	}

}

/*
// TestGetIndex tests and GetIndex func to see if the request is redirected
func TestGetCommentsHandlerStatusCodes(t *testing.T) {

	tt := []struct {
		name         string
		route        string
		method       string
		expectedCode int
	}{
		{"GET Index", "/comments", "GET", 200},
		{"POST Index", "/comments", "POST", 200},
		{"PUT Index", "/comments", "PUT", 200},
		{"PATCH Index", "/comments", "PATCH", 200},
		{"DELETE Index", "/comments", "DELETE", 200},
	}

	// message check loops over message struct checking for the env var and logs debug message if not found
	for _, tc := range tt {
		// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
		// pass 'nil' as the third parameter.
		req, err := http.NewRequest(tc.method, tc.route, nil)
		if err != nil {
			t.Fatal(err)
		}

		// TODO add functions tables tests`
		// Create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(GetAllComments)

		// Handlers satisfy http.Handler, call their ServeHTTP method
		// directly and pass in the Request and ResponseRecorder.
		handler.ServeHTTP(rr, req)
		if rr.Code != tc.expectedCode {
			t.Errorf(tc.name+" test case: handler returned wrong status code: got %v want %v", rr.Code, tc.expectedCode)
		}
	}
}
*/
