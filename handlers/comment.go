// meetupcomments/handlers/comment.go

// Comment handlers are associated with comment r.HandleFunc routes in main.go. Business logic happens here for each
//+of the routes, including template execution and data passing, and the handlers can call other functions, including Data Access Objects
//+if data from the database is needed.

// LICENSE GNU GPL v3.0

//--------------------------------------------------------------------------------//

package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/ryanthebossross/meetupcomments/dao"
	"gitlab.com/ryanthebossross/meetupcomments/models"
)

// look to env for port
var servicePort = os.Getenv("servicePortENV")

// get dns to resolve
var dnsName = os.Getenv("dnsNameENV")

////////////
// CREATE //
////////////

// PostComment writes a comment to the DB and redirects
func PostComment(w http.ResponseWriter, r *http.Request) {
	// listen for requests and parse the json
	decoder := json.NewDecoder(r.Body)

	// defene closing the connection
	defer r.Body.Close()

	// assign var to hold comment model
	var c = models.Comment{}

	// decode the json request into a reference to the model
	err := decoder.Decode(&c)
	if err != nil {
		log.Println(err)
	}
	log.Println(c)

	// TODO: time formatting.
	//Set created and update time
	c.Created = time.Now().Local()
	c.Updated = time.Now().Local()

	// call the insert one dao with form values
	dao.InsertOneComment(c)

	// redirect back to show page.
	http.Redirect(w, r, "http://"+dnsName+"/comments", 301)
}

//////////
// READ //
//////////

// GetIndex handler redirects site index to /comments index for RESTful routing
func GetIndex(w http.ResponseWriter, r *http.Request) {
	// redirect to show page.
	http.Redirect(w, r, "http://"+dnsName+":"+servicePort+"/comments", 301)
}

// GetAllComments handler renders the /comments index template
func GetAllComments(w http.ResponseWriter, r *http.Request) {
	// use the dao and get all the comments from DB
	d := dao.GetAllComments()

	// send a json response and check the error
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	err := json.NewEncoder(w).Encode(d)
	// check the error
	if err != nil {
		log.Fatalln(err)
	}
}

/*
//GetComment retrieves a comment to be edited.
func GetComment(w http.ResponseWriter, r *http.Request) {
	// Grab username
	id := p.ByName("id")

	// Init the user model interface
	c := models.Comment{}

	uj, err := json.Marshal(d)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(w, "%s\n", uj)
}
*/
