# meetupcomments/Dockerfile

# Multi-Stage Dockerfile to build a scratch Docker Image of the app 

# LICENSE GNU GPL v3.0

##--------------------------------------------------------------------------------##

##################
### test stage ###
##################
# pull in the golang 1.12.1 alpine image 
FROM golang:1.12.1

# set the working directory
WORKDIR /go/src/main

# add the project files into the builder container
COPY . .

# install app dependencies
RUN go get -d -v ./...

# run unit tests
# RUN go test -v ./...

#####################
### builder stage ###
#####################

# pull in the golang 1.12.1 alpine image 
FROM golang:1.12.1 AS builder

# set the working directory
WORKDIR /go/src/main

# add the project files into the builder container
COPY . .

# install app dependencies
RUN go get -d -v ./...

# build the app with compiler flags for os, arch, and to remove debug information for smaller images
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o main

###################     
### final stage ###
###################

## create minimal scratch image
FROM scratch

## set the working directory
WORKDIR /go/bin/main

## copy the combiled binary to the smaller image
COPY --from=builder /go/src/main /go/bin/main

## start the binary when the container starts
ENTRYPOINT ["./main"]
