# meetupcomments/Makefile

# Makefile to make life easier with some of these longer commands

# LICENSE GNU GPL v3.0

##--------------------------------------------------------------------------------##

# build the multi container image
build:
	$(clean_command):
	docker build -t meetupcomments:dev .

# start docker-compose
up:
	$(clean_command):
	docker-compose up -d

run:
	$(clean_command):
	docker run -it --rm -d --name meetupcomments golang:1.12.1-alpine

# connect to mongo in the yelpcamp db using local auth.
mongo:
	$(clean_command):
	mongo mongodb://root:root@localhost:27017/meetup?authSource=admin

# generage bearer token for kubernetes dashboard
bearer:
	$(clean_command):
	kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
